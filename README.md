CLASE DE ROS - USTA (HH)
==============

I.¿Cómo usar el package usb_cam?
--------------

**Si está utilizando máquina virtual VirtualBox siga los siguientes pasos**

1. Ingresamos a la páguina https://www.virtualbox.org/wiki/Downloads
2. Descargarmos los "Extension Pack" a través del link "All supported platforms"
3. Abrimos VirtualBox
4. Vamos a la opción "Archivo" -> "Preferencias..." del VirtualBox
5. Seleccionamos la opción "Extensiones"
6. y añadimos la "Extension Pack" como nuevo paquete
7. A partir de este momento la máquina virtual puede ver y utilizar la cámara integrada del portatil 

> para más información vean el siguiente video: https://www.youtube.com/watch?time_continue=136&v=ElTRFk_Mw10&feature=emb_title

II.¿Cómo ejecutar el nodo usb_cam_node?
--------------

1. Abrimos un nuevo terminal y ejecutamos el roscore
``` 
$ roscore
```
2. Abrimos un segundo terminal y ejecutamos el nodo correspondiente
``` 
$ rosrun usb_cam usb_cam_node
```
3. A través de la herramienta (nodo) rqt_image_view verificamos el correcto funcionamiento de nuestro nodo

Opción 1 (Contracción de nodo en ROS)
``` 
$ rqt_image_view
```

Opción 2 (Instrucción completa)
``` 
$ rosrun rqt_image_view rqt_image_view
```

> Nota: La herramienta rqt_image_view por defecto no está asociada a ningun Topic. Por lo cual debemos seleccionar el topic que queremos ver


TEMPLATE PARA GENERAR EL README.md GIT
==============

*This will be Italic*

**This will be Bold**

- This will be a list item
- This will be a list item

1. This will be a numerated list 
2. This will be a numerated list 

``` 
this will be a code segment
```

> this will be a definition

<http://url> this will be a web link

<!--this will a comment-->

This will a title
--------------